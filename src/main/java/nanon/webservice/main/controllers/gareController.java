package nanon.webservice.main.controllers;

import nanon.webservice.main.models.Gare;
import nanon.webservice.main.services.JSONSave;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class gareController {

    @GetMapping("/gares")
    public List<Gare> ListGares () {
        JSONSave saver = new JSONSave();

        for (Gare gare : Gare.gares) {
            try {
                saver.save(gare);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Gare.gares;
    }

    @RequestMapping(value = {"/gares/{id}"})
    public Gare Gare (@PathVariable(name = "id") int gareId) {
        return Gare.gares.get(gareId);
    }
}
