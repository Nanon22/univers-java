package nanon.webservice.main.controllers;

import nanon.webservice.main.models.Train;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class trainController {

    @GetMapping("/trains")
    @Cacheable("Trains")
    public List<Train> ListTrains () {
        return Train.trains;
    }

    @RequestMapping(value = {"/trains/{id}"})
    @Cacheable("Trains")
    public Train train (@PathVariable(name = "id") int trainId) {
        return Train.trains.get(trainId);
    }
}
