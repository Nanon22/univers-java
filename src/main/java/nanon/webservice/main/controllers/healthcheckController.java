package nanon.webservice.main.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class healthcheckController {

    @GetMapping("/healthcheck")
    public String Ok () {
        return "ok";
    }
}
