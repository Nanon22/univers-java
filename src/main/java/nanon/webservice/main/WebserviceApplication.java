package nanon.webservice.main;

import nanon.webservice.main.models.Gare;
import nanon.webservice.main.models.Train;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;
import java.time.Month;

@SpringBootApplication
public class WebserviceApplication {
	Gare Noisy = new Gare("Noisy le Grand, Mont d'Est");
	Gare PlaceItalie = new Gare("Place d'Italie");
	Gare Nation = new Gare("Nation");

	LocalDateTime aDateTime = LocalDateTime.of(2020, Month.SEPTEMBER, 15, 19, 30, 40);

	LocalDateTime aDateTime2 = LocalDateTime.of(2015, Month.SEPTEMBER, 15, 20, 30, 40);

	Train RER = new Train("Bobigny", 207, aDateTime, aDateTime2);

	Train RER2 = new Train("Marne la vallée", 407, aDateTime, aDateTime2);

	Train RER3 = new Train("Torcy", 607, aDateTime, aDateTime2);

	public static void main(String[] args) {
		SpringApplication.run(WebserviceApplication.class, args);
	}

}
